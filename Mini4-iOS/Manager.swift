import MultipeerConnectivity

protocol ManagerDelegate {
	func clientState(_ state:ConnectivityClientState)
	func gameState(_ state:PeerState)
	func newIndex(_ index:Int)
}

class Manager:NSObject,ConnectivityClientDelegate {
	override init() {
		super.init()
		client.delegate = self
	}

	let client = ConnectivityClient()
	var delegate:ManagerDelegate?

	func updated(state: ConnectivityClientState) {
		delegate?.clientState(state)
	}

	func received(command: String) {
		if command.hasPrefix("I") {
			if let index = Int(command.substring(start:1)) {
				delegate?.newIndex(index)
			}
		} else if command.hasPrefix("S") {
			if let state = PeerState(rawValue:command.substring(start:1)) {
				delegate?.gameState(state)
			}
		}
    }

	func send(swipe:Direction) {
		switch swipe {
		case .up: client.send(command:"S0")
		case .down: client.send(command:"S1")
		case .left: client.send(command:"S2")
		case .right: client.send(command:"S3")
		default: break
		}
	}

	func send(eyesX x:Double,eyesY y:Double) {
		client.send(command:"E\(x)|\(y)")
	}
}
