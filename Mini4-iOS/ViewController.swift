import UIKit
import MultipeerConnectivity
import CoreMotion

class ViewController:UIViewController,ManagerDelegate {
    @IBOutlet weak var tv: UIImageView!
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var buttonLoading: UIButton!

	@IBOutlet weak var arrowUp: UIImageView!
	@IBOutlet weak var arrowDown: UIImageView!
	@IBOutlet weak var arrowLeft: UIImageView!
	@IBOutlet weak var arrowRight: UIImageView!

	@IBOutlet weak var upperText: UILabel!
	@IBOutlet weak var middleText: UILabel!
	@IBOutlet weak var lowerText: UILabel!
	@IBOutlet weak var warningText: UILabel!

    @IBOutlet weak var buttonLeave: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    let manager = Manager()
	let motionManager = CMMotionManager()

    let backgroundDuration:Double = 0.2
    let contentDuration:Double = 0.1

    var connected = false
    var index:Int = 0
    var clientState:ConnectivityClientState = .disconnected
    var gameState:PeerState? = nil

	override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        swipeStart()
        view.backgroundColor = UIColor.clear
        activity.startAnimating()
		motionManager.accelerometerUpdateInterval = 0.1
        if buttonLoading.layer.animation(forKey:"rotate") == nil {
            let rot = CABasicAnimation(keyPath:"transform.rotation")
            rot.fromValue = 0.0
            rot.toValue = -2*Float(M_PI)
            rot.duration = 4
            rot.repeatCount = Float.infinity
            buttonLoading.layer.add(rot,forKey:"rotate")
        }
        setContent(animated:false)
        manager.delegate = self
        manager.client.lookForServer()
    }

    private func swipeStart() {
        func add(_ dir:UISwipeGestureRecognizerDirection) {
            let swipe = UISwipeGestureRecognizer(target:self,action:#selector(self.respondToSwipeGesture(swipe:)))
            swipe.direction = dir
            view.addGestureRecognizer(swipe)
        }
        add(.up)
        add(.down)
        add(.left)
        add(.right)
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        motionManager.startAccelerometerUpdates(to:OperationQueue.main) {
            (data:CMAccelerometerData?,error:Error?) in
            if let a = data?.acceleration {
                self.manager.send(eyesX:a.x,eyesY:a.y)
            }
        }
    }

	override func viewWillDisappear(_ animated: Bool) {
		manager.client.disconnect()
		motionManager.stopAccelerometerUpdates()
	}

	func clientState(_ state:ConnectivityClientState) {
        clientState = state
        setContent()
	}

	func gameState(_ state:PeerState) {
        gameState = state
        setContent()
	}

	func newIndex(_ index:Int) {
        self.index = index
		setBackground()
	}

	func setBackground(animated:Bool = true) {
        guard index >= 0 && index <= Colour.chars.count else { return }
		let action:(() -> Void) = {
			self.view.layer.backgroundColor = Colour.chars[self.index].cgColor
		}
		if animated {
            UIView.animate(withDuration:backgroundDuration,animations:action)
		} else {
			action()
		}
	}

    func setContent(animated:Bool = true) {
        var tvPlay = false
        var tvLoading = false
        var leave = false
        var activity = false
        var arrowH = false
        var arrowV = false
        var upper:String?
        var middle:String?
        var lower:String?

        if clientState == .connected {
            connected = true
        } else if connected || !animated {
            connected = false
            gameState = nil
            index = 1+Utils.random(4)
            setBackground(animated:animated)
        }

        switch clientState {
        case .disconnected:
            tvPlay = true
            upper = "you have disconnected from the game."
            lower = "press play to connect!"
        case .couldntFindServer:
            tvPlay = true
            upper = "couldn't find a nearby Apple TV."
            lower = "press play to connect!"
        case .serverDown:
            tvPlay = true
            upper = "lost connection to the Apple TV."
            lower = "press play to connect!"
        case .lookingForServer:
            tvLoading = true
            lower = "searching for Apple TV..."
        case .connected:
            if let state = gameState {
                leave = true
                switch state {
                case .game:
                    arrowH = true
                    arrowV = true
                case .gameLose:
                    middle = "YOU LOSE!\n\nyour ghost is playing for you now. wait until the end of this game!"
                case .menu:
                    arrowH = true
                    upper = "waiting for the game to start..."
                    lower = "you can change your\ncharacter's color by\nswiping left and right!"
                case .waitingFull:
                    middle = "THE ROOM IS FULL!\n\nwait for somebody to leave before you can play!"
                case .waitingGame:
                    middle = "WE'RE IN THE MIDDLE OF A GAME!\n\nwait until the end of this game!"
                }
            } else {
                activity = true
            }
        }

        let upperAlpha:CGFloat
        let middleAlpha:CGFloat
        let lowerAlpha:CGFloat
        if upper != nil {
            upperText.text = upper
            upperAlpha = 1
        } else {
            upperAlpha = 0
        }
        if middle != nil {
            middleText.text = middle
            middleAlpha = 1
        } else {
            middleAlpha = 0
        }
        if lower != nil {
            lowerText.text = lower
            lowerAlpha = 1
        } else {
            lowerAlpha = 0
        }

        let action:(() -> Void) = {
            self.tv.alpha = (tvPlay || tvLoading) ? 1 : 0
            self.buttonPlay.alpha = tvPlay ? 1 : 0
            self.buttonLoading.alpha = tvLoading ? 1 : 0
            self.buttonLeave.alpha = leave ? 1 : 0
            self.activity.alpha = activity ? 1 : 0
            self.arrowUp.alpha = arrowV ? 0.5 : 0
            self.arrowDown.alpha = arrowV ? 0.5 : 0
            self.arrowLeft.alpha = arrowH ? 0.5 : 0
            self.arrowRight.alpha = arrowH ? 0.5 : 0
            self.upperText.alpha = upperAlpha
            self.middleText.alpha = middleAlpha
            self.lowerText.alpha = lowerAlpha
			self.warningText.alpha = self.tv.alpha
        }
        if animated {
            UIView.animate(withDuration:contentDuration,animations:action)
        } else {
            action()
        }
    }

    func respondToSwipeGesture(swipe:UISwipeGestureRecognizer) {
        switch swipe.direction {
		case UISwipeGestureRecognizerDirection.up: manager.send(swipe:.up)
        case UISwipeGestureRecognizerDirection.down: manager.send(swipe:.down)
        case UISwipeGestureRecognizerDirection.left: manager.send(swipe:.left)
        case UISwipeGestureRecognizerDirection.right: manager.send(swipe:.right)
        default: break
        }
    }

	@IBAction func exitButton(_ sender: Any) {
        guard connected else { return }
		let alert = UIAlertController(title: "Quit this game?", message: "Are you sure you want to quit the game?", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "no", style: .cancel, handler: nil))
		alert.addAction(UIAlertAction(title: "yeah!", style: .destructive, handler: { action in
            if action.style == .destructive {
                self.manager.client.disconnect()
            }
		}))
		present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func syncButton(_ sender: Any) {
        if clientState == .lookingForServer {
            manager.client.stopLookingForServer()
        } else if clientState != .connected {
            manager.client.lookForServer()
        }
    }
}
