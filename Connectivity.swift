import Foundation
import MultipeerConnectivity

class Connectivity:NSObject {
	let peerID:MCPeerID
	private(set) var session:MCSession!

	let serviceType = "manjotvservice"

	init(name:String) {
		peerID = MCPeerID(displayName:name)
	}

	internal func send(command:String,to peers:[MCPeerID]) {
		guard let session = session else { return }
		guard !peers.isEmpty else { return }
		//NSLog("%@","sendCommand: \(command)")
		guard let data = command.data(using:.utf8,allowLossyConversion:false) else { return }
		do {
			try session.send(data,toPeers:peers,with:.reliable)
		} catch let error as NSError {
			//NSLog("%@","\(error)")
			log("send command error: \(error)")
		}
	}

	internal func send(commands:[String],to peers:[MCPeerID]) {
		send(command:commands.joined(separator:"/"),to:peers)
	}

	internal func update(peer:MCPeerID,to state:MCSessionState) {
		//
	}

	internal func receive(command:String,from peer:MCPeerID) {
		//
	}

	internal func beginSession() {
		guard session == nil else { return }
		session = MCSession(peer:peerID,securityIdentity:nil,encryptionPreference:.required)
		session.delegate = self
	}

	internal func endSession() {
		guard let s = session else { return }
		s.disconnect()
		session = nil
	}

	internal func log(_ text:String) {
		print("[connectivity] \(text)")
	}
}

extension Connectivity:MCSessionDelegate {
	func session(_ session:MCSession,peer peerID:MCPeerID,didChange state:MCSessionState) {
		OperationQueue.main.addOperation { () -> Void in
			guard self.session != nil else { return }
			self.update(peer:peerID,to:state)
		}
	}

	func session(_ session:MCSession,didReceive data:Data,fromPeer peerID:MCPeerID) {
		guard let command = String(data:data,encoding:.utf8) else { return }
		OperationQueue.main.addOperation { () -> Void in
			guard self.session != nil else { return }
			let split = command.components(separatedBy:"/")
			for i in split {
				self.receive(command:i,from:peerID)
			}
		}
	}

	func session(_ session:MCSession,didReceive stream:InputStream,withName streamName:String,fromPeer peerID:MCPeerID) {}
	func session(_ session:MCSession,didFinishReceivingResourceWithName resourceName:String,fromPeer peerID:MCPeerID,at localURL:URL,withError error:Error?) {}
	func session(_ session:MCSession,didStartReceivingResourceWithName resourceName:String,fromPeer peerID:MCPeerID,with progress:Progress) {}
}
