import SpriteKit
import GameController

class RemoteInput {
	private(set) var touch = CGPoint.zero
	private(set) var deltaTouch = CGPoint.zero
	private var touchStart = CGPoint.zero
	private var currentTouch:UITouch?

	private var statePress = Set<Button>()
	private var stateHold = Set<Button>()
	private var stateRelease = Set<Button>()

	private(set) var swipe:Direction = .none

	func press(_ button:Button) -> Bool {
		return statePress.contains(button)
	}

	func hold(_ button:Button) -> Bool {
		return stateHold.contains(button)
	}

	func release(_ button:Button) -> Bool {
		return stateRelease.contains(button)
	}

	func start(_ view:UIView) {
		func add(_ dir:UISwipeGestureRecognizerDirection) {
			let swipe = UISwipeGestureRecognizer(target:self,action:#selector(self.evaluate(swipe:)))
			swipe.direction = dir
			view.addGestureRecognizer(swipe)
		}
		add(.up)
		add(.down)
		add(.left)
		add(.right)
	}

	func update() {
		for i in GCController.controllers() {
			if let a = i.motion?.gravity {
				Game.bar.rawPlayerNodes[0].character.look(x:CGFloat(a.x),y:CGFloat(a.y))
				break
			}
		}
	}

	func lateUpdate() {
		deltaTouch = CGPoint.zero
		statePress.removeAll()
		stateRelease.removeAll()
		swipe = .none
	}

	func evaluate(button:Button,began:Bool) {
		if began {
			statePress.insert(button)
			stateHold.insert(button)
		} else {
			stateRelease.insert(button)
			stateHold.remove(button)
		}
	}

	func evaluate(presses:Set<UIPress>) {
		for press in presses {
			let button:Button
			switch press.type {
			case .select: button = .click
			case .menu: button = .menu
			case .playPause: button = .playPause
			default: continue
			}
			evaluate(button:button,began:!(press.phase == .ended || press.phase == .cancelled))
		}
	}

	func evaluate(touches:Set<UITouch>) {
		func touchPosition(_ rawTouch:CGPoint,first:Bool) {
			var newTouch:CGPoint
			if first {
				touchStart = rawTouch
				newTouch = CGPoint.zero
			} else {
				newTouch = CGPoint(x:rawTouch.x-touchStart.x,y:rawTouch.y-touchStart.y)
				deltaTouch = CGPoint(x:deltaTouch.x+newTouch.x-touch.x,y:deltaTouch.y+newTouch.y-touch.y)
			}
			touch = newTouch
		}
		if let touch = currentTouch {
			if touches.contains(touch) {
				touchPosition(touch.location(in:Game.scene),first:false)
				if touch.phase == .cancelled || touch.phase == .ended {
					currentTouch = nil
					evaluate(button:.touch,began:false)
				}
			}
		} else {
			for touch in touches {
				if touch.phase != .cancelled && touch.phase != .ended {
					currentTouch = touch
					touchPosition(touch.location(in:Game.scene),first:true)
					evaluate(button:.touch,began:true)
					break
				}
			}
		}
	}

	@objc func evaluate(swipe:UISwipeGestureRecognizer) {
		switch swipe.direction {
		case UISwipeGestureRecognizerDirection.up: self.swipe = .up
		case UISwipeGestureRecognizerDirection.down: self.swipe = .down
		case UISwipeGestureRecognizerDirection.left: self.swipe = .left
		case UISwipeGestureRecognizerDirection.right: self.swipe = .right
		default: break
		}
	}
}
