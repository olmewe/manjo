import Foundation
import MultipeerConnectivity

class RoomManager:ConnectivityServerDelegate {
	let server = ConnectivityServer()

	private var peerById:[MCPeerID:Peer] = [:]
	private var peerByIndex:[Int:Peer] = [:]

	private var pendingPeers:[MCPeerID] = []

	var lockPeers = false {
		didSet {
			for (k,_) in peerById {
				send(state:expectedPeerState,to:k)
			}
			if oldValue && !lockPeers {
				unlockPeers()
			}
		}
	}

	var expectedPeerState:PeerState {
		return lockPeers ? .game : .menu
	}

	init() {
		server.delegate = self
	}

	func lateUpdate() {
		for (_,v) in peerByIndex {
			v.swipe = .none
		}
	}

	//public functions

	func name(_ index:Int = 0) -> String? {
		if index == 0 {
			return "remote control"
		}
		return peerByIndex[index]?.name
	}

	func swipe(_ index:Int = 0) -> Direction {
		if index == 0 {
			return Game.input.swipe
		}
		return peerByIndex[index]?.swipe ?? .none
	}

	func exists(_ index:Int = 0) -> Bool {
		return index == 0 || peerByIndex[index] != nil
	}

	func connected(_ index:Int = 0) -> Bool {
		return index == 0 || peerByIndex[index]?.connected == true
	}

	//managing connections and disconnections

	func connected(peer id:MCPeerID) {
		if let peer = peerById[id] {
			if !peer.connected {
				peer.connected = true
				Game.bar.updatePlayers()
                if lockPeers {
                    if Game.bar.rawPlayerNodes[peer.index].alive {
                        send(state:.game,to:id,index:peer.index)
                    } else {
                        send(state:.gameLose,to:id,index:peer.index)
                    }
                } else {
                    send(state:.menu,to:id,index:peer.index)
                }
			}
			return
		}
		if let peer = tryAdd(peer:id) {
			Game.bar.updatePlayers()
			send(state:expectedPeerState,to:id,index:peer.index)
			return
		}
		if !pendingPeers.contains(id) {
            print("[pending player] \(id.displayName)")
			pendingPeers.append(id)
		}
		if lockPeers {
			send(state:.waitingGame,to:id)
		} else {
			send(state:.waitingFull,to:id)
		}
	}

	func connecting(peer:MCPeerID) {
		//uhh
	}

	func disconnected(peer id:MCPeerID) {
		var needsUpdate = false
		if let peer = peerById[id] {
			if lockPeers {
				if peer.connected {
					peer.connected = false
					needsUpdate = true
				}
			} else {
				peerById.removeValue(forKey:id)
				peerByIndex.removeValue(forKey:peer.index)
				needsUpdate = true
			}
		}
		if let i = pendingPeers.index(of:id) {
			pendingPeers.remove(at:i)
		}
		if !pendingPeers.isEmpty,let peer = tryAdd(peer:pendingPeers[0]) {
			send(state:expectedPeerState,to:pendingPeers.removeFirst(),index:peer.index)
			needsUpdate = true
		}
		if needsUpdate {
			Game.bar.updatePlayers()
		}
	}

	private func tryAdd(peer id:MCPeerID) -> Peer? {
		if lockPeers { return nil }
		for i in 1..<Game.maxPlayers {
			if peerByIndex[i] == nil {
				let peer = Peer(peerID:id)
				peer.index = i
				peerById[id] = peer
				peerByIndex[i] = peer
				return peer
			}
		}
		return nil
	}

	private func unlockPeers() {
		var needsUpdate = false
		let peers = peerByIndex.map { $0.value }
		for i in peers {
			if !i.connected {
				peerById.removeValue(forKey:i.peerID)
				peerByIndex.removeValue(forKey:i.index)
				needsUpdate = true
			}
		}
		while !pendingPeers.isEmpty,let peer = tryAdd(peer:pendingPeers[0]) {
			send(state:expectedPeerState,to:pendingPeers.removeFirst(),index:peer.index)
			needsUpdate = true
		}
		if needsUpdate {
			Game.bar.updatePlayers()
		}
	}

	private func move(peer:Peer,to direction:Direction) {
		let newIndex:Int
		switch direction {
		case .left: newIndex = peer.index-1
		case .right: newIndex = peer.index+1
		default: return
		}
		if newIndex <= 0 || newIndex >= Game.maxPlayers { return }
		if let otherPeer = peerByIndex[newIndex] {
			peerByIndex[peer.index] = otherPeer
			otherPeer.index = peer.index
			send(index:otherPeer.index)
		} else {
			peerByIndex.removeValue(forKey:peer.index)
		}
		peerByIndex[newIndex] = peer
		peer.index = newIndex
		send(index:peer.index)
		Game.bar.updatePlayers()
	}

	//receiving commands

	func received(command:String,from id:MCPeerID) {
		guard let peer = peerById[id] else { return }
		let swipe:Direction
		switch command {
		case "S0": swipe = .up
		case "S1": swipe = .down
		case "S2": swipe = .left
		case "S3": swipe = .right
		default: swipe = .none
		}
		if swipe != .none {
			peer.swipe = swipe
			if !lockPeers {
				move(peer:peer,to:swipe)
			}
			return
		}
		if command.hasPrefix("E") {
			let c = command.substring(start:1).components(separatedBy:"|")
			if c.count < 2 { return }
			if let x = Double(c[0]),let y = Double(c[1]) {
				Game.bar.rawPlayerNodes[peer.index].character.look(x:CGFloat(x),y:CGFloat(y))
			}
		}
	}

	//sending commands

	func send(index:Int) {
		guard let peer = peerByIndex[index] else { return }
		server.send(command:"I\(index)",to:[peer.peerID])
	}

	func send(state:PeerState,to id:MCPeerID,index:Int? = nil) {
		let command = "S\(state.rawValue)"
		if let i = index {
			server.send(commands:[command,"I\(i)"],to:[id])
		} else {
			server.send(command:command,to:[id])
		}
	}

	func send(lose index:Int) {
		if let id = peerByIndex[index]?.peerID {
			send(state:.gameLose,to:id)
		}
	}
}
