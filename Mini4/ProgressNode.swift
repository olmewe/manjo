import SpriteKit

class ProgressNode:SKNode {
	let dots:[SKShapeNode]
	let radius:CGFloat
	let black:Bool

	//top radius: 16
	//bottom radius: 9
	init(dots dotCount:Int,radius:CGFloat,black:Bool) {
		var dotsTemp:[SKShapeNode] = []
		let offset = (CGFloat(dotCount)-1)*0.5
		for i in 0..<dotCount {
			let dot = SKShapeNode(circleOfRadius:radius)
			dot.fillColor = black ? Colour.black : Colour.white
			dot.position = CGPoint(x:(CGFloat(i)-offset)*radius*3.2,y:0)
			dotsTemp.append(dot)
		}
		dots = dotsTemp
		self.radius = radius
		self.black = black
		super.init()
		for i in dots {
			addChild(i)
		}
	}

	required init?(coder aDecoder:NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func set(only index:Int) {
		for (n,i) in dots.enumerated() {
			i.alpha = (n == index) ? 1 : (black ? Colour.inactiveAlpha : (Colour.inactiveAlpha*2))
		}
	}

	func set(length:Int) {
		for (n,i) in dots.enumerated() {
			i.alpha = (n < length) ? 1 : (black ? Colour.inactiveAlpha : (Colour.inactiveAlpha*2))
		}
	}
}
