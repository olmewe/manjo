import SpriteKit

class GameScene:BaseScene {
	var gameNode:SKNode!
	var botRootNode:SKNode!
	let botNode = CharacterNode(bot:true)
	var progressRootNode:SKNode!
	var progressNode:ProgressNode!
    var sequenceNode:SKLabelNode!
    var highScoreNode:SKNode!
    var highScoreLabelNode:SKLabelNode!

	var tipNode:[SKNode] = []
	var tipMultNode:[SKNode] = []

	var endBgNode:SKNode!
	var endBgNodes:[SKShapeNode] = []

	var endNode:SKNode!
	var endCrownNode:SKSpriteNode!
	var endCharacterNode:SKNode!
	var endCharacterCompNode:SKSpriteNode!
	var endScoreNode:SKLabelNode!
	var endNewHighScoreNode:SKSpriteNode!
	var endContinueNode:SKLabelNode!

	var shakeProgress:CGFloat = 0

	var botTurn = true
	var turnTempo:CGFloat = 0
	var botTempo:CGFloat = 0
	var botDuration:CGFloat = 0
	var ghostTempo:CGFloat = 0
	var endGame = false
	var endGameTempo:CGFloat = 0
	var transitionTempo:CGFloat = 0
	var transitioning = false

	var sequence:[Action] = []
	var currentIndex:Int = 0
	var maxIndex:Int = 0
	var sequenceIndexes:[Int] = []
    var highScore:Int = 0
	var isNewHighScore = false

	var currentTip:Int = -1

	var playerCount:Int = 0

	static let lifesSolo:Int = 3
	static let lifesCoop:Int = 5
	static let lifesComp:Int = 3
	var lastPlayer:Int = 0

	var totalLifes:Int = 0
	var currentLifes:[Int] = []
	var connectedPlayers:[Bool] = []

	static func create() -> SKScene {
		return GameScene(fileNamed:"GameScene")!
	}

	override func start() {
		gameNode = childNode(withName:"/game")
		botRootNode = gameNode.childNode(withName:"botRoot")
		botRootNode.addChild(botNode)
		botNode.position = CGPoint.zero
		progressRootNode = gameNode.childNode(withName:"progressRoot")
		sequenceNode = gameNode.childNode(withName:"sequence") as? SKLabelNode
		sequenceNode.color = Colour.gray
        highScoreNode = gameNode.childNode(withName:"highScore")
        highScoreLabelNode = highScoreNode.childNode(withName:"title") as? SKLabelNode

		tipNode.append(gameNode.childNode(withName:"tipFirst")!)
		tipNode.append(gameNode.childNode(withName:"tipSecond")!)
		tipMultNode.append(gameNode.childNode(withName:"tipFirstMult")!)
		tipMultNode.append(gameNode.childNode(withName:"tipSecondMult")!)
		for i in tipNode+tipMultNode {
			i.alpha = 0
			for c in i.children {
				(c as? SKLabelNode)?.fontColor = Colour.grayEvenDarker
			}
		}

		endBgNode = childNode(withName:"/endBg")
		endBgNode.isHidden = true
		for i in 0..<4 {
			let circle = SKShapeNode(circleOfRadius:1)
			circle.lineWidth = 0
			circle.fillColor = ((i & 1) == 0) ? Colour.white : Colour.grayBrighter
			circle.position = CGPoint.zero
			circle.xScale = 0
			circle.yScale = 0
			endBgNode.addChild(circle)
			endBgNodes.append(circle)
		}
		endNode = childNode(withName:"/end")
		endNode.isHidden = true
		endCrownNode = endNode.childNode(withName:"crown") as? SKSpriteNode
		endCharacterNode = endNode.childNode(withName:"character")
		endScoreNode = endNode.childNode(withName:"score") as? SKLabelNode
		endNewHighScoreNode = endNode.childNode(withName:"newHighScore") as? SKSpriteNode
		endContinueNode = endNode.childNode(withName:"continue") as? SKLabelNode
	}

	override func show() {
		Game.inGame = true
		Game.bar.setAreas()
		Game.bar.resetPoses()
		playerCount = Game.bar.playerCount
		var endPoses = CharacterNode.framesEnd
		func spawnEndCharacter(colour:UIColor,at index:Int,count:Int) -> SKSpriteNode {
			let node = SKSpriteNode(texture:endPoses.remove(at:Utils.random(endPoses.count)))
			node.size = CGSize(width:CharacterNode.prop*475,height:475)
			node.position = CGPoint(x:(CGFloat(index)-(CGFloat(count)-1)/2)*210,y:210)
			if index <= count/2 {
				node.zPosition = CGFloat(index)
			} else {
				node.zPosition = CGFloat(count-index-1)
			}
			node.color = colour
			node.colorBlendFactor = 1
			endCharacterNode.addChild(node)
			return node
		}
		if Game.mode == .coop || Game.mode == .coopHard {
			for i in 0..<playerCount {
				endCharacterCompNode = spawnEndCharacter(colour:Game.bar.playerNodes[i].colour,at:i,count:playerCount)
			}
		} else {
			endCharacterCompNode = spawnEndCharacter(colour:Colour.black,at:0,count:1)
		}
		if Game.mode == .solo || Game.mode == .coop {
			totalLifes = (Game.mode == .solo) ? GameScene.lifesSolo : GameScene.lifesCoop
			progressNode = ProgressNode(dots:totalLifes,radius:16,black:true)
			progressRootNode.addChild(progressNode)
			progressNode.position = CGPoint.zero
			progressNode.set(length:totalLifes)
		} else {
			if Game.mode == .comp {
				totalLifes = GameScene.lifesComp
			} else {
				totalLifes = 1
            }
            for i in Game.bar.playerNodes {
                i.progress.set(length:totalLifes)
            }
			if progressNode != nil {
				progressNode.removeFromParent()
				progressNode = nil
			}
		}
		currentLifes.removeAll()
		connectedPlayers.removeAll()
		for _ in 0..<playerCount {
			currentLifes.append(totalLifes)
			connectedPlayers.append(true)
		}
		turnTempo = 2
		sequence.removeAll()
		currentIndex = 0
		maxIndex = 1
		updateText()
		gameNode.alpha = 0
	}

	override func update() {
		if gameNode.alpha < 1 {
			gameNode.alpha += Game.delta
			if gameNode.alpha > 1 { gameNode.alpha = 1 }
		}
		botNode.update()
		if shakeProgress > 0 {
			shakeProgress -= Game.delta
			if shakeProgress < 0 { shakeProgress = 0 }
			progressNode?.position = Utils.shake(shakeProgress)
		}
		func checkGhost() {
			if !connectedPlayers[sequence[currentIndex].player] || currentLifes[sequence[currentIndex].player] <= 0 {
				ghostTempo = Utils.random()+0.5
			}
		}
		for i in 0..<tipNode.count {
			if currentTip == i {
				if tipNode[i].alpha < 1 {
					tipNode[i].alpha += Game.delta
					if tipNode[i].alpha > 1 { tipNode[i].alpha = 1 }
					if playerCount > 1 {
						tipMultNode[i].alpha = tipNode[i].alpha
					}
				}
			} else {
				if tipNode[i].alpha > 0 {
					tipNode[i].alpha -= Game.delta*2
					if tipNode[i].alpha < 0 { tipNode[i].alpha = 0 }
					if playerCount > 1 {
						tipMultNode[i].alpha = tipNode[i].alpha
					}
				}
			}
		}
		if !endGame {
			var needsCheckEndGame = false
			for i in 0..<playerCount {
				let b = Game.bar.playerNodes[i].connected
				if connectedPlayers[i] != b {
					connectedPlayers[i] = b
					if b {
						if currentLifes[i] > 0 {
							Game.bar.playerNodes[i].set(ghost:false)
						}
					} else {
						Game.bar.playerNodes[i].set(ghost:true)
						needsCheckEndGame = true
					}
				}
			}
			if needsCheckEndGame {
				checkEndGame()
				if endGame { return }
			}
			if turnTempo > 0 {
				//turn interval
				turnTempo -= Game.delta
				if turnTempo <= 0 {
					turnTempo = 0
					currentIndex = 0
					if botTurn {
						//beginning of a bot turn
						print("bot turn begin!")
						botTempo = botDuration
						maxIndex = sequence.count+1
						highScore = maxIndex-1
						if maxIndex <= 2 {
							botDuration = 1
						} else {
							botDuration = 0.6/(CGFloat(sequence.count)*0.3/CGFloat(playerCount)+1)+0.4
						}
					} else {
						//beggining of a player turn
						print("player turn begin!")
						maxIndex = sequence.count
						checkGhost()
					}
					updateText()
					if !botTurn && maxIndex <= tipNode.count {
						currentTip = maxIndex-1
					} else {
						botNode.poseBot(.none,player:0)
					}
					for i in Game.bar.playerNodes {
						i.character.pose(.none)
					}
				}
			}
			if turnTempo == 0 {
				if botTurn {
					botTempo -= Game.delta
					if botTempo <= 0 {
						//bot turn pose
						if currentIndex < sequence.count {
							let action = sequence[currentIndex]
							botNode.poseBot(action.direction,player:action.player)
						} else {
							if sequenceIndexes.isEmpty {
								for i in 0..<playerCount {
									if currentLifes[i] > 0 {
										sequenceIndexes.append(i)
									}
								}
							}
							let player = sequenceIndexes.remove(at:Utils.random(sequenceIndexes.count))
							let directionInt = Utils.random(4)
							let direction:Direction
							switch directionInt {
							case 0: direction = .up
							case 1: direction = .down
							case 2: direction = .left
							default: direction = .right
							}
							sequence.append(Action(direction:direction,player:player))
							botNode.poseBot(direction,player:player)
						}
						currentIndex += 1
						updateText()
						if currentIndex >= maxIndex {
							botTurn = false
							print("bot turn end!")
							turnTempo = 1
						} else {
							botTempo = botDuration
						}
					}
				} else {
					//player turn pose
					var getInput = false
					var inputPlayer:Int = 0
					var inputDirection:Direction = .none
					for (n,i) in Game.bar.playerNodes.enumerated() {
						if !connectedPlayers[n] || currentLifes[n] <= 0 { continue }
						let swipe = i.swipe
						if swipe != .none {
							getInput = true
							inputPlayer = n
							inputDirection = swipe
						}
					}
					var doPose = false
					if getInput {
						let action = sequence[currentIndex]
						if action.player == inputPlayer && action.direction == inputDirection {
							doPose = true
						} else {
							Game.bar.playerNodes[inputPlayer].shake()
							if Game.mode == .comp || Game.mode == .compHard {
								loseCompLife(inputPlayer)
							} else {
								shakeProgress = 1
								loseCoopLife()
							}
							checkGhost()
						}
					} else if ghostTempo > 0 {
						ghostTempo -= Game.delta
						if ghostTempo <= 0 {
							ghostTempo = 0
							let action = sequence[currentIndex]
							if !connectedPlayers[action.player] || currentLifes[action.player] <= 0 {
								inputPlayer = action.player
								inputDirection = action.direction
								doPose = true
							}
						}
					}
					if doPose {
						Game.bar.playerNodes[inputPlayer].character.pose(inputDirection)
						currentIndex += 1
						updateText()
						if currentIndex >= maxIndex {
							botTurn = true
							print("player turn end!")
							turnTempo = 1
							currentTip = -1
						} else {
							checkGhost()
						}
					}
				}
			}
		} else {
			let maxTempo:CGFloat = 5
			let scales:[CGFloat] = [1150,820,500,270]
			if endGameTempo < maxTempo {
				endGameTempo += Game.delta
				if endGameTempo > maxTempo { endGameTempo = maxTempo }
				endBgNode.isHidden = false
				for (n,i) in endBgNodes.enumerated() {
					let s = Utils.easeOut(endGameTempo-1.5-0.3*CGFloat(n))*scales[n]
					i.xScale = s
					i.yScale = s
				}
				endNode.isHidden = false
				endScoreNode.position = CGPoint(x:endScoreNode.position.x,y:Utils.lerp(from:-675,to:-430,with:Utils.easeOut(endGameTempo-3)/0.8))
				let cs = Utils.easeOut((endGameTempo-2.25)/0.7)
				endCharacterNode.xScale = cs
				endCharacterNode.yScale = cs
				if isNewHighScore {
					let s = Utils.bounce((endGameTempo-maxTempo+0.5)/0.5)*0.6
					endNewHighScoreNode.xScale = s
					endNewHighScoreNode.yScale = s
				} else {
					endNewHighScoreNode.isHidden = true
				}
				if Game.mode == .comp || Game.mode == .compHard {
					let t:[CGFloat] = [3,3.6,4]
					let y:[CGFloat] = [600,200,225]
					if endGameTempo < t[0] {
						endCrownNode.position = CGPoint(x:endCrownNode.position.x,y:y[0])
					} else if endGameTempo < t[1] {
						endCrownNode.position = CGPoint(x:endCrownNode.position.x,y:Utils.lerp(from:y[0],to:y[1],with:Utils.easeIn((endGameTempo-t[0])/(t[1]-t[0]))))
					} else if endGameTempo < t[2] {
						endCrownNode.position = CGPoint(x:endCrownNode.position.x,y:Utils.lerp(from:y[1],to:y[2],with:Utils.easeOut((endGameTempo-t[1])/(t[2]-t[1]))))
					} else {
						endCrownNode.position = CGPoint(x:endCrownNode.position.x,y:y[2])
					}
				} else {
					endCrownNode.isHidden = true
				}
			}
			endContinueNode.alpha = Utils.easeOut((endGameTempo-4.5)/0.5)*(0.75+sin(Game.time*3)*0.25)
			if !transitioning && endGameTempo > 4.5 && (Game.input.release(.click) || Game.input.release(.playPause)) {
				transitioning = true
				transitionTempo = 1
				gameNode.isHidden = true
                Game.audio.playSoundEffect(filename: "play.wav")
			}
			if transitioning && transitionTempo > 0 {
				transitionTempo -= Game.delta/1.5
				if transitionTempo <= 0 {
					transitionTempo = 0
					Game.show(scene:MenuScene.create())
				}
				let t = transitionTempo*1.1-0.1
				for (n,i) in endBgNodes.enumerated() {
					let s = Utils.lerp(from:0,to:scales[n],with:(Utils.easeOut(t)-CGFloat(n)*0.1)/0.7)
					i.xScale = s
					i.yScale = s
				}
				endNode.alpha = Utils.easeIn(transitionTempo*2-1)
			}
		}
	}

    func setHighScore() {
		var loadedScore = Game.loadScore(for:Game.mode)
		if loadedScore < highScore {
			loadedScore = highScore
			Game.saveScore(highScore,for:Game.mode)
			isNewHighScore = true
        }
        highScoreLabelNode.text = "\(loadedScore)"
    }

	func loseCoopLife() {
		print("everyone loses a life!")
		for i in 0..<playerCount {
			if currentLifes[i] <= 0 { continue }
			currentLifes[i] -= 1
			if currentLifes[i] <= 0 {
				Game.bar.playerNodes[i].set(ghost:true)
			}
		}
		progressNode?.set(length:currentLifes[0])
		checkEndGame()
	}

	func loseCompLife(_ index:Int) {
		print("player #\(index) loses a life!")
		currentLifes[index] -= 1
		Game.bar.playerNodes[index].progress.set(length:currentLifes[index])
		if currentLifes[index] <= 0 {
			lastPlayer = index
			Game.bar.playerNodes[index].set(ghost:true)
			print("player #\(index) is out")
			if let i = sequenceIndexes.index(of:index) {
				sequenceIndexes.remove(at:i)
			}
			checkEndGame()
			if !endGame {
				Game.bar.send(lose:index)
			}
		}
	}

	func checkEndGame() {
		for i in 0..<playerCount {
			if connectedPlayers[i] && currentLifes[i] > 0 { return }
		}

//        print(Game.userDefaults.object(forKey: (Game.mode.rawValue))!)
//        print(Game.mode.rawValue)

		print("end game")
		endGame = true
		endGameTempo = 0
		let finalScore = "\(highScore)"
		endScoreNode.text = finalScore
		endNewHighScoreNode.position = CGPoint(x:20+CGFloat(finalScore.characters.count)*40,y:endNewHighScoreNode.position.y)
		if Game.mode == .comp || Game.mode == .compHard {
			let c = Game.bar.playerNodes[lastPlayer].colour
			endCharacterCompNode.color = c
			endCrownNode.color = c
		}
	}

	func updateText() {
		sequenceNode.text = "\(currentIndex) / \(maxIndex)"
        setHighScore()
	}
}
