import UIKit
import SpriteKit
import GameplayKit

class GameViewController:UIViewController {
	override func viewDidLoad() {
		super.viewDidLoad()
		Game.start(self.view as! SKView)
		Game.show(scene:IntroScene.create())
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	override func touchesBegan(_ touches:Set<UITouch>,with event:UIEvent?) {
		super.touchesBegan(touches,with:event)
		Game.input.evaluate(touches:touches)
	}

	override func touchesMoved(_ touches:Set<UITouch>,with event:UIEvent?) {
		super.touchesMoved(touches,with:event)
		Game.input.evaluate(touches:touches)
	}

	override func touchesEnded(_ touches:Set<UITouch>,with event:UIEvent?) {
		super.touchesEnded(touches,with:event)
		Game.input.evaluate(touches:touches)
	}

	override func touchesCancelled(_ touches:Set<UITouch>,with event:UIEvent?) {
		super.touchesCancelled(touches,with:event)
		Game.input.evaluate(touches:touches)
	}

	override func pressesBegan(_ presses:Set<UIPress>,with event:UIPressesEvent?) {
		super.pressesBegan(presses,with:event)
		Game.input.evaluate(presses:presses)
	}

	override func pressesEnded(_ presses:Set<UIPress>,with event:UIPressesEvent?) {
		super.pressesEnded(presses,with:event)
		Game.input.evaluate(presses:presses)
	}
}
