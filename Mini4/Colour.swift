import SpriteKit

class Colour {
	static let black = hex(0x151415)
	static let grayEvenDarker = hex(0xaaaaaa)
	static let grayDarker = hex(0xcfcece)
	static let gray = hex(0xd8d8d7)
	static let grayBrighter = hex(0xeeeeee)
	static let white = hex(0xffffff)
	static let red = hex(0xc85551)
	static let blue = hex(0x7ebbdb)
	static let yellow = hex(0xeddd64)
	static let green = hex(0x90ba88)

	static let chars = [
		black,
		red,
		blue,
		yellow,
		green,
	]

	static var inactiveAlpha:CGFloat = {
		var n:CGFloat = 0
		var a:CGFloat = 0
		var b:CGFloat = 1
		var t:CGFloat = 0.5
		white.getRed(&a,green:&n,blue:&n,alpha:&n)
		black.getRed(&b,green:&n,blue:&n,alpha:&n)
		gray.getRed(&t,green:&n,blue:&n,alpha:&n)
		return (t-a)/(b-a)
	}()

	static func hex(_ i:Int,a:CGFloat = 1) -> UIColor {
		return UIColor(red:CGFloat((i>>16)&0xff)/255,green:CGFloat((i>>8)&0xff)/255,blue:CGFloat(i&0xff)/255,alpha:a)
	}

	static func rgb(_ r:Int,_ g:Int,_ b:Int,a:CGFloat = 1) -> UIColor {
		return UIColor(red:CGFloat(r)/255,green:CGFloat(g)/255,blue:CGFloat(b)/255,alpha:a)
	}

	static func lerp(from a:UIColor,to b:UIColor,t:CGFloat) -> UIColor {
		if t <= 0 { return a }
		if t >= 1 { return b }
		var ar:CGFloat = 0
		var ag:CGFloat = 0
		var ab:CGFloat = 0
		var aa:CGFloat = 0
		var br:CGFloat = 0
		var bg:CGFloat = 0
		var bb:CGFloat = 0
		var ba:CGFloat = 0
		a.getRed(&ar,green:&ag,blue:&ab,alpha:&aa)
		b.getRed(&br,green:&bg,blue:&bb,alpha:&ba)
		let ti = 1-t
		return UIColor(red:ar*ti+br*t,green:ag*ti+bg*t,blue:ab*ti+bb*t,alpha:aa*ti+ba*t)
	}
}
