import SpriteKit

class PlayerBarNode:SKNode {
	let rawPlayerNodes:[PlayerNode]
	var playerNodes:[PlayerNode] = []
	var playerCount:Int {
		return playerNodes.count
	}

	private let inactiveWidth:CGFloat = 0.065

	override init() {
		var nodes = [PlayerNode]()
		for i in 0..<Game.maxPlayers {
			nodes.append(PlayerNode(index:i))
		}
		rawPlayerNodes = nodes
		super.init()
		for i in rawPlayerNodes {
			addChild(i)
		}
		setActivePlayers()
		setAreas(start:true)
	}

	required init?(coder aDecoder:NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func attach(to scene:SKScene) {
		removeFromParent()
		scene.addChild(self)
		position = CGPoint.zero
	}

	func update() {
		for i in rawPlayerNodes {
			i.update()
		}
	}

	func setActivePlayers() {
		playerNodes.removeAll()
		for i in rawPlayerNodes {
			if i.exists {
				playerNodes.append(i)
			}
		}
	}

	func setAreas(start:Bool = false) {
		let activeWidth:CGFloat
		let inactiveWidth:CGFloat
		if Game.inGame {
			activeWidth = 1/CGFloat(playerNodes.count)
			inactiveWidth = 0
		} else {
			activeWidth = (1-CGFloat(rawPlayerNodes.count-playerNodes.count)*self.inactiveWidth)/CGFloat(playerNodes.count)
			inactiveWidth = self.inactiveWidth
		}
		let showProgress = Game.inGame && Game.mode == .comp
		var x:CGFloat = 0
		for i in rawPlayerNodes {
			let a = i.exists
			let w = a ? activeWidth : inactiveWidth
			if start {
				i.set(area:Area(x:x,y:0,w:w,h:0),animate:false)
			} else {
				i.showCharacter = a
				i.showLabel = a
				i.showProgress = a && showProgress
				i.showLoading = !a
				i.set(area:Area(x:x,y:0,w:w,h:0.5),animate:true)
				i.updateName()
			}
			x += w
		}
	}

	func animateArea(_ index:Int) {
		let a = rawPlayerNodes[index].area
		rawPlayerNodes[index].set(area:Area(x:a.xa,y:a.ya,w:a.w,h:0.5),animate:true)
	}

	func updatePlayers() {
		setActivePlayers()
		setAreas()
	}

	func resetPoses() {
		for i in rawPlayerNodes {
            i.alive = true
			i.set(ghost:false)
			i.character.pose(.none)
		}
	}

	func send(lose index:Int) {
        playerNodes[index].alive = false
		Game.room.send(lose:playerNodes[index].index)
	}
}
