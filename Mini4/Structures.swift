import SpriteKit
import MultipeerConnectivity


class Utils {
	#if os(tvOS)
	static func shake(_ t:CGFloat) -> CGPoint {
		return CGPoint(x:Game.size.height*0.03*t*t*sin(CGFloat(M_PI)*8*t),y:0)
	}
	#endif

	static func clamp(_ t:CGFloat,between a:CGFloat,and b:CGFloat) -> CGFloat {
		if t <= a { return a }
		if t >= b { return b }
		return t
	}

	static func clamp01(_ t:CGFloat) -> CGFloat {
		if t <= 0 { return 0 }
		if t >= 1 { return 1 }
		return t
	}

	static func lerp(from a:CGFloat,to b:CGFloat,with t:CGFloat) -> CGFloat {
		if t <= 0 { return a }
		if t >= 1 { return b }
		return a*(1-t)+b*t
	}

	static func easeIn(_ t:CGFloat) -> CGFloat {
		if t <= 0 { return 0 }
		if t >= 1 { return 1 }
		return t*t
	}

	static func easeOut(_ t:CGFloat) -> CGFloat {
		if t <= 0 { return 0 }
		if t >= 1 { return 1 }
		return (2-t)*t
	}

	static func ease(_ t:CGFloat) -> CGFloat {
		if t <= 0 { return 0 }
		if t >= 1 { return 1 }
		return t*t*(3-2*t)
	}

	static func bounce(_ t:CGFloat) -> CGFloat {
		if t <= 0 { return 0 }
		if t >= 1 { return 1 }
		return (sin(t*CGFloat(M_PI)*(0.2+2.5*t*t*t))*pow(1-t,2.2)+t)*(1+(1.2*(1-t)))
	}

	static func random(_ length:Int) -> Int {
		return Int(arc4random_uniform(UInt32(length)))
	}

	static func random() -> CGFloat {
		return CGFloat(arc4random())/CGFloat(UINT32_MAX)
	}
}


enum Direction {
	case none
	case up
	case down
	case left
	case right
}

enum Button {
	case touch
	case click
	case menu
	case playPause
}

enum GameMode:String {
	case solo = "solo"
	case soloHard = "soloHard"
	case coop = "coop"
	case coopHard = "coopHard"
	case comp = "comp"
	case compHard = "compHard"
}

enum PeerState:String {
	case menu
	case game
	case gameLose
	case waitingFull
	case waitingGame
}

struct Action {
	var direction:Direction
	var player:Int
	init(direction:Direction,player:Int = 0) {
		self.direction = direction
		self.player = player
	}
}

struct Area {
	let xa:CGFloat
	let xb:CGFloat
	let ya:CGFloat
	let yb:CGFloat
	let x:CGFloat
	let y:CGFloat
	let w:CGFloat
	let h:CGFloat
	init(xa:CGFloat,xb:CGFloat,ya:CGFloat,yb:CGFloat) {
		self.xa = xa
		self.xb = xb
		self.ya = ya
		self.yb = yb
		self.x = (xa+xb)*0.5
		self.y = (ya+yb)*0.5
		self.w = xb-xa
		self.h = yb-ya
	}
	init(x xa:CGFloat,y ya:CGFloat,w:CGFloat,h:CGFloat) {
		self.xa = xa
		self.xb = xa+w
		self.ya = ya
		self.yb = ya+h
		self.x = xa+w*0.5
		self.y = ya+h*0.5
		self.w = w
		self.h = h
	}
	static let zero = Area(x:0,y:0,w:0,h:0)
	static let full = Area(x:0,y:0,w:1,h:1)
}

class Peer {
	let peerID:MCPeerID
	let name:String

	var connected = true
	var index:Int = 0
	var swipe:Direction = .none

	init(peerID:MCPeerID) {
		self.peerID = peerID
		name = peerID.displayName.lowercased()
	}
}

extension String {
	func substring(start:Int) -> String {
		return substring(from:index(startIndex,offsetBy:String.IndexDistance(start)))
	}
}
