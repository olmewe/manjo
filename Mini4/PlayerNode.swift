import SpriteKit

class PlayerNode:SKCropNode {
	let index:Int
	var exists:Bool {
		return Game.room.exists(index)
	}
	var connected:Bool {
		return Game.room.connected(index)
	}
	var swipe:Direction {
		return Game.room.swipe(index)
	}
    var alive = true

	var colour:UIColor {
		return background.fillColor
	}
	var showLoading = false
	var showCharacter = false
	var showLabel = false
	var showProgress = false
	var connectedTempo:CGFloat = 0

	private let mask = SKShapeNode(rectOf:CGSize(width:1,height:1))
	private let background = SKShapeNode(rectOf:CGSize(width:1,height:1))
	private let loading = SKSpriteNode(imageNamed:"loading")
	let character:CharacterNode
	private let label = SKLabelNode(fontNamed:"ArialRoundedMTBold")
	let progress = ProgressNode(dots:GameScene.lifesComp,radius:9,black:false)

	private(set) var area = Area.zero
	static let areaDuration:CGFloat = 0.6
	private var areaTempo:CGFloat = 0
	private var areaFrom = Area.zero
	private var areaNow = Area.zero

	private var shakeTempo:CGFloat = 0

	init(index:Int) {
		self.index = index
		character = CharacterNode(bot:false,colour:Colour.chars[index])
		super.init()
		addChild(background)
		addChild(loading)
		addChild(character)
		addChild(label)
		addChild(progress)
		mask.position = CGPoint.zero
		mask.fillColor = Colour.white
		mask.lineWidth = 0
		mask.zPosition = 100
		#if arch(i386) || arch(x86_64)
		#else
			maskNode = mask
		#endif
		loading.position = CGPoint.zero
		let s = Game.size.height*0.08
		loading.size = CGSize(width:s,height:s)
		loading.alpha = 0
		background.position = CGPoint.zero
		background.fillColor = Colour.chars[index]
		background.lineWidth = 0
		character.position = CGPoint.zero
		character.alpha = 0
		label.fontSize = Game.size.height*0.03
		label.position = CGPoint(x:0,y:-Game.size.height*0.175)
		label.horizontalAlignmentMode = .center
		label.verticalAlignmentMode = .center
		label.fontColor = Colour.white
		label.alpha = 0
		progress.position = CGPoint(x:0,y:Game.size.height*0.165)
		progress.alpha = 0
	}

	required init?(coder aDecoder:NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func update() {
		if connected {
			if connectedTempo < 1 {
				connectedTempo += Game.delta*4
				if connectedTempo > 1 { connectedTempo = 1 }
			}
		} else {
			if connectedTempo > 0 {
				connectedTempo -= Game.delta*4
				if connectedTempo < 0 { connectedTempo = 0 }
			}
		}
		updateAlpha(show:showLoading,node:loading,velocity:2,max:0.5)
		updateAlpha(show:showCharacter,node:character)
		updateAlpha(show:showLabel,node:label,velocity:2)
		updateAlpha(show:showProgress,node:progress)
		if connectedTempo < 1 {
			label.alpha = Utils.lerp(from:label.alpha*(0.7+sin(Game.time*6)*0.15),to:label.alpha,with:connectedTempo)
		}
		if loading.alpha > 0 {
			loading.zRotation = Game.time*1.2
		}
		if areaTempo > 0 {
			areaTempo -= Game.delta/PlayerNode.areaDuration
			if areaTempo < 0 { areaTempo = 0 }
			let t = Utils.ease(areaTempo)
			let ti = 1-t
			areaNow = Area(
				xa:areaFrom.xa*t+area.xa*ti,
				xb:areaFrom.xb*t+area.xb*ti,
				ya:areaFrom.ya*t+area.ya*ti,
				yb:areaFrom.yb*t+area.yb*ti
			)
			updateDisplay()
		}
		character.update()
		if shakeTempo > 0 {
			shakeTempo -= Game.delta
			if shakeTempo < 0 { shakeTempo = 0 }
			let pos = Utils.shake(shakeTempo)
			character.position = pos
			progress.position = CGPoint(x:pos.x,y:progress.position.y)
		}
	}

	private func updateAlpha(show:Bool,node:SKNode,velocity:CGFloat = 1,max:CGFloat = 1) {
		if show {
			if node.alpha < max {
				node.alpha += max*velocity*Game.delta/PlayerNode.areaDuration
				if node.alpha > max { node.alpha = max }
			}
		} else {
			if node.alpha > 0 {
				node.alpha -= max*velocity*Game.delta/PlayerNode.areaDuration
				if node.alpha < 0 { node.alpha = 0 }
			}
		}
	}

	private func updateDisplay() {
		if areaNow.w <= 0 || areaNow.h <= 0 {
			isHidden = true
			return
		}
		isHidden = false
		position = CGPoint(x:Game.size.width*(areaNow.x-0.5),y:Game.size.height*(areaNow.y-0.5))
		background.xScale = Game.size.width*areaNow.w
		background.yScale = Game.size.height*areaNow.h
		mask.xScale = background.xScale
		mask.yScale = background.yScale
	}

	func set(area:Area,animate:Bool) {
		if animate {
			areaTempo = 1
			areaFrom = areaNow
		} else {
			areaTempo = 0
			areaNow = area
			updateDisplay()
		}
		self.area = area
	}

	func set(ghost:Bool) {
		character.set(ghost:ghost)
	}

	func updateName() {
		if let name = Game.room.name(index) {
			if label.parent == nil {
				addChild(label)
			}
			label.text = name
		}
	}

	func shake() {
		shakeTempo = 1
        Game.audio.playSoundEffect(filename: "missed.wav")
	}
}
