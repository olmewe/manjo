import SpriteKit

class CharacterNode:SKNode {
	private let bot:Bool
	private let height:CGFloat
	var direction:Direction {
		return posing ? lastDirection : .none
	}
	private(set) var ghost = false

	private let sprite = SKSpriteNode()
	private let eyes = SKSpriteNode()
	static let prop:CGFloat = 300/380

	private let puff = SKSpriteNode(imageNamed:"puff")

	private var lastDirection:Direction = .up
	private var posing = false

	private var transitionTempo:CGFloat = 0
	private let transitionDuration:CGFloat = 0.5
	private var returnTempo:CGFloat = 0
	private var puffTempo:CGFloat = 0
	private var blinkTempo:CGFloat = 0
	private var blinkWait:CGFloat = 0
	private let blinkDuration:CGFloat = 0.1

	private var eyesRawX:CGFloat = 0
	private var eyesRawY:CGFloat = 0
	private var eyesX:CGFloat = 0
	private var eyesY:CGFloat = 0
	private var eyesNormX:CGFloat = 0
	private var eyesNormY:CGFloat = 0
	private var eyesDistX:CGFloat = 0.05
	private var eyesDistY:CGFloat = 0.05

	private var tintColour = Colour.white

	static let framesCharacter:[SKTexture] = [
		SKTexture(imageNamed:"character/idle"),
		SKTexture(imageNamed:"character/up"),
		SKTexture(imageNamed:"character/down"),
		SKTexture(imageNamed:"character/left"),
		SKTexture(imageNamed:"character/right"),
	]

	static let framesGhost:[SKTexture] = [
		SKTexture(imageNamed:"ghost/idle"),
		SKTexture(imageNamed:"ghost/up"),
		SKTexture(imageNamed:"ghost/down"),
		SKTexture(imageNamed:"ghost/left"),
		SKTexture(imageNamed:"ghost/right"),
	]

	static let framesEyes:[SKTexture] = [
		SKTexture(imageNamed:"eyes/idle"),
		SKTexture(imageNamed:"eyes/up"),
		SKTexture(imageNamed:"eyes/down"),
		SKTexture(imageNamed:"eyes/left"),
		SKTexture(imageNamed:"eyes/right"),
	]

	static let framesEyesClosed:[SKTexture] = [
		SKTexture(imageNamed:"eyesclosed/idle"),
		SKTexture(imageNamed:"eyesclosed/up"),
		SKTexture(imageNamed:"eyesclosed/down"),
		SKTexture(imageNamed:"eyesclosed/left"),
		SKTexture(imageNamed:"eyesclosed/right"),
	]

	static let framesEnd:[SKTexture] = [
		SKTexture(imageNamed:"end/0"),
		SKTexture(imageNamed:"end/1"),
		SKTexture(imageNamed:"end/2"),
		SKTexture(imageNamed:"end/3"),
		SKTexture(imageNamed:"end/4"),
	]

	init(bot:Bool = true,colour:UIColor = Colour.white) {
		self.bot = bot
		height = Game.size.height*(bot ? 0.375 : 0.3)
		super.init()
		addChild(sprite)
		addChild(eyes)
		addChild(puff)
		puff.alpha = 0
		sprite.zPosition = 0
		eyes.zPosition = 1
		puff.zPosition = 2
		if bot {
			sprite.colorBlendFactor = 1
		} else {
			eyes.color = colour
			eyes.colorBlendFactor = 1
		}
		blink()
		updateDisplay()
		updatePosition()
		updateEyes()
	}

	required init?(coder aDecoder:NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func update() {
		var update = false
		if transitionTempo > 0 {
			transitionTempo -= Game.delta
			if transitionTempo < 0 { transitionTempo = 0 }
			update = true
		}
		if returnTempo > 0 {
			returnTempo -= Game.delta
			if returnTempo <= 0 {
				returnTempo = 0
				if posing {
					if bot {
						poseBot(.none,player:0)
					} else {
						pose(.none)
					}
					update = false
				}
			}
		}
		let blinking = blinkTempo < blinkDuration
		blinkTempo += Game.delta
		if blinkTempo >= blinkWait {
			blink()
			update = true
		} else if blinking != (blinkTempo < blinkDuration) {
			update = true
		}
		if update {
			updateDisplay()
		}
		if puffTempo > 0 {
			puffTempo -= Game.delta*2
			if puffTempo < 0 { puffTempo = 0 }
			puff.alpha = puffTempo
		}
		updatePosition()
		eyesX = Utils.lerp(from:eyesX,to:eyesRawX,with:Game.delta*8)
		eyesY = Utils.lerp(from:eyesY,to:eyesRawY,with:Game.delta*8)
		eyesNormX = Utils.lerp(from:eyesNormX,to:eyesX,with:Game.delta*0.25)
		eyesNormY = Utils.lerp(from:eyesNormY,to:eyesY,with:Game.delta*0.25)
		updateEyes()
	}

	private func updatePosition() {
		var y = sprite.size.height*0.05
		if ghost {
			y += sin(Game.time*2)*height*0.02
		}
		sprite.position = CGPoint(x:0,y:y)
	}

	private func updateEyes() {
		eyes.position = CGPoint(
			x:sprite.position.x+(eyesX-eyesNormX)*eyesDistX*height,
			y:sprite.position.y+(eyesY-eyesNormY)*eyesDistY*height
		)
	}

	private func updateDisplay() {
		let h:CGFloat
		if direction != .none {
			let t = transitionTempo/transitionDuration
			h = height*(1+t*t*t*0.125)
		} else {
			h = height
		}
		let s = CGSize(width:CharacterNode.prop*h,height:h)
		sprite.size = s
		eyes.size = s
		puff.size = s

		let i:Int
		switch direction {
		case .up: i = 1; eyesDistX = 0.02; eyesDistY = 0.03
		case .down: i = 2; eyesDistX = 0.05; eyesDistY = 0.02
		case .left: i = 3; eyesDistX = 0.008; eyesDistY = 0.002
		case .right: i = 4; eyesDistX = 0.035; eyesDistY = 0.035
		default: i = 0; eyesDistX = 0.05; eyesDistY = 0.05
		}
		if ghost {
			sprite.texture = CharacterNode.framesGhost[i]
			eyes.isHidden = true
		} else {
			sprite.texture = CharacterNode.framesCharacter[i]
			eyes.texture = (blinkTempo < blinkDuration) ? CharacterNode.framesEyesClosed[i] : CharacterNode.framesEyes[i]
			eyes.isHidden = false
		}

		if bot {
			if posing {
				sprite.color = tintColour
			} else if transitionTempo > 0 {
				sprite.color = Colour.lerp(from:Colour.gray,to:tintColour,t:transitionTempo/transitionDuration)
			} else {
				sprite.color = Colour.gray
			}
		}

	}

	func pose(_ direction:Direction) {
		if direction == .none {
			if !posing { return }
			posing = false
		} else {
			posing = true
			lastDirection = direction
		}
		transitionTempo = transitionDuration
		updateDisplay()
		updateEyes()
        playSFX(direction)
	}

	func poseBot(_ direction:Direction,player:Int,for tempo:CGFloat = 0) {
		if direction == .none {
			if !posing { return }
			posing = false
		} else {
			posing = true
			lastDirection = direction
			if player >= 0 && player < Game.bar.playerCount {
				tintColour = Game.bar.playerNodes[player].colour
			} else {
				tintColour = Colour.gray
			}
		}
		transitionTempo = transitionDuration
		returnTempo = tempo
		updateDisplay()
		updateEyes()
        playSFX(direction)
	}

    func playSFX(_:Direction, isDead:Bool=false) {
        switch direction {
        case .left:
            Game.audio.playSoundEffect(filename: "left.wav")
        case .right:
            Game.audio.playSoundEffect(filename: "right.wav")
        case .up:
            Game.audio.playSoundEffect(filename: "up.wav")
        case .down:
            Game.audio.playSoundEffect(filename: "down.wav")
        default:
            break
        }
    }

	func set(ghost:Bool) {
		if self.ghost == ghost { return }
		self.ghost = ghost
		if ghost {
			sprite.alpha = bot ? Colour.inactiveAlpha : (Colour.inactiveAlpha*2)
		} else {
			sprite.alpha = 1
        }
        Game.audio.playSoundEffect(filename: "dying.wav")
		puffTempo = 1
		updateDisplay()
		updateEyes()
	}

	func blink() {
		blinkTempo = 0
		blinkWait = blinkDuration+Utils.lerp(from:1,to:5,with:Utils.random())
		updateDisplay()
	}

	func look(x:CGFloat,y:CGFloat) {
		let x = Utils.clamp(x,between:-1,and:1)
		let y = Utils.clamp(y,between:-1,and:1)
		let d = x*x+y*y
		if d > 1 {
			let sd = 1/sqrt(d)
			eyesRawX = x*sd
			eyesRawY = y*sd
		} else {
			eyesRawX = x
			eyesRawY = y
		}
	}
}
