import SpriteKit

class IntroScene:BaseScene {
	var titleNode:SKSpriteNode!
	var characterNode:SKSpriteNode!
	var eyesNode:SKSpriteNode!
	let whiteNode = SKShapeNode(rect:CGRect(x:-Game.size.width/2,y:0,width:Game.size.width,height:Game.size.height))

	var tempo:CGFloat = 0
	var areaIndex:Int = 0

	var eyesX:CGFloat = 0
	var eyesY:CGFloat = 0

	static func create() -> SKScene {
		return IntroScene(fileNamed:"IntroScene")!
	}

	override func start() {
		titleNode = childNode(withName:"/title") as? SKSpriteNode
		characterNode = childNode(withName:"/character") as? SKSpriteNode
		eyesNode = characterNode.childNode(withName:"eyes") as? SKSpriteNode
		addChild(whiteNode)
		whiteNode.lineWidth = 0
		whiteNode.fillColor = Colour.white
	}

	override func show() {
		tempo = 0
	}

	override func update() {
		let maxTempo:CGFloat = 7
		if tempo >= maxTempo { return }
		tempo += Game.delta

		//animate character and logo
		let titleAnim:(t:[CGFloat],x:[CGFloat]) = ([2,3],[1400,150])
		let characterAnim:(t:[CGFloat],x:[CGFloat]) = ([2.72,3.1],[0,-340])
		if tempo < titleAnim.t[0] {
			titleNode.position = CGPoint(x:titleAnim.x[0],y:titleNode.position.y)
		} else if tempo < titleAnim.t[1] {
			titleNode.position = CGPoint(x:Utils.lerp(from:titleAnim.x[0],to:titleAnim.x[1],with:Utils.ease((tempo-titleAnim.t[0])/(titleAnim.t[1]-titleAnim.t[0]))),y:titleNode.position.y)
		} else {
			titleNode.position = CGPoint(x:titleAnim.x[1],y:titleNode.position.y)
		}
		if tempo < characterAnim.t[0] {
			characterNode.position = CGPoint(x:characterAnim.x[0],y:characterNode.position.y)
		} else if tempo < characterAnim.t[1] {
			characterNode.position = CGPoint(x:Utils.lerp(from:characterAnim.x[0],to:characterAnim.x[1],with:Utils.easeOut((tempo-characterAnim.t[0])/(characterAnim.t[1]-characterAnim.t[0]))),y:characterNode.position.y)
		} else {
			characterNode.position = CGPoint(x:characterAnim.x[1],y:characterNode.position.y)
		}

		//animate eyes
		let ex:CGFloat
		let ey:CGFloat
		if tempo < 1.8 {
			ex = 0
			ey = 0
		} else if tempo < 2.8 {
			ex = -0.5
			ey = 0.2
		} else if tempo < 4 {
			ex = 0.6
			ey = -0.1
		} else {
			ex = 0
			ey = 0
		}
		eyesX = Utils.lerp(from:eyesX,to:ex,with:Game.delta*6)
		eyesY = Utils.lerp(from:eyesY,to:ey,with:Game.delta*6)
		let c:CGFloat = 50
		eyesNode.position = CGPoint(x:eyesX*c,y:eyesY*c)

		//set alpha
		let a = Utils.easeOut(maxTempo-1-tempo)
		titleNode.alpha = a
		characterNode.alpha = a

		//animate colour regions
		whiteNode.position = CGPoint(x:0,y:Utils.lerp(from:Game.size.height/2,to:0,with:Utils.ease((tempo-maxTempo+1)/PlayerNode.areaDuration)))
		while areaIndex < Game.bar.rawPlayerNodes.count && tempo > maxTempo-1+CGFloat(areaIndex)*0.04 {
			Game.bar.animateArea(areaIndex)
			areaIndex += 1
		}

		//change screen when done
		if tempo >= maxTempo {
			Game.room.server.start()
			Game.show(scene:MenuScene.create())
		}
	}
}
