import SpriteKit

class MenuScene:BaseScene {
	var playNode:SKNode!
	var modeNode:SKNode!
	var highScoreNode:SKNode!
	var highScoreLabelNode:SKLabelNode!
	var arrowLeftNode:SKSpriteNode!
	var arrowRightNode:SKSpriteNode!
	var modeNodes:[SKNode] = []
	var modes:[GameMode] = []
	var currentMode:Int {
		get {
			if Game.bar.playerCount > 1 {
				return MenuScene.multMode
			} else {
				return MenuScene.soloMode
			}
		}
		set {
			if Game.bar.playerCount > 1 {
				var v = (newValue-2)%4
				if v < 0 { v += 4 }
				MenuScene.multMode = v+2
			} else {
				var v = newValue%2
				if v < 0 { v += 2 }
				MenuScene.soloMode = v
			}
		}
	}
	static var multMode:Int = 2
	static var soloMode:Int = 0

	var solo = true

	var sceneTempo:CGFloat = 0
	var fadeTempo:CGFloat = 0
	var modeSelect = false
	var modeTempo:CGFloat = 0
	var leftTempo:CGFloat = 0
	var rightTempo:CGFloat = 0
	var leaving = false

	var arrowLeftStart:CGFloat = 0
	var arrowRightStart:CGFloat = 0

	static var firstTime = true

	static func create() -> SKScene {
		return MenuScene(fileNamed:"MenuScene")!
	}

	override func start() {
		playNode = childNode(withName:"/play")
		(playNode.childNode(withName:"icon") as? SKSpriteNode)?.color = Colour.grayDarker
		(playNode.childNode(withName:"text") as? SKLabelNode)?.fontColor = Colour.grayDarker
		modeNode = childNode(withName:"/mode")
		arrowLeftNode = modeNode.childNode(withName:"arrowLeft") as? SKSpriteNode
		arrowRightNode = modeNode.childNode(withName:"arrowRight") as? SKSpriteNode
		highScoreNode = modeNode.childNode(withName: "highScore")
		highScoreLabelNode = highScoreNode.childNode(withName: "title") as? SKLabelNode
		arrowLeftStart = arrowLeftNode.position.x
		arrowRightStart = arrowRightNode.position.x
		arrowLeftNode.color = Colour.gray
		arrowRightNode.color = Colour.gray
		func addMode(_ mode:GameMode) {
			let node:SKNode! = modeNode.childNode(withName:mode.rawValue)
			(node.childNode(withName:"title") as? SKLabelNode)?.fontColor = Colour.black
			(node.childNode(withName:"subtitle") as? SKLabelNode)?.fontColor = Colour.black
			(node.childNode(withName:"desc") as? SKLabelNode)?.fontColor = Colour.black
			modeNodes.append(node)
			modes.append(mode)
		}
		addMode(.solo)
		addMode(.soloHard)
		addMode(.coop)
		addMode(.comp)
		addMode(.coopHard)
		addMode(.compHard)
	}

	override func show() {
		Game.inGame = false
		Game.bar.setAreas()
		Game.bar.resetPoses()
		sceneTempo = 0
		if MenuScene.firstTime {
			fadeTempo = 0
			modeSelect = false
			MenuScene.firstTime = false
		} else {
			fadeTempo = 1
			modeSelect = true
		}
		playNode.alpha = 0
		modeNode.alpha = 0
		solo = Game.bar.playerCount <= 1
		selectMode(0,animate:false)
	}

	override func update() {
		func setAlpha() {
			let t = Utils.easeOut(sceneTempo*1.1-0.1)
			if fadeTempo > 0.5 {
				playNode.alpha = 0
				modeNode.alpha = (fadeTempo*2-1)*t
			} else {
				playNode.alpha = (1-fadeTempo*2)*t
				modeNode.alpha = 0
			}
		}
		let currentSolo = Game.bar.playerCount <= 1
		if solo != currentSolo {
			solo = currentSolo
			selectMode(0,animate:false)
		}
		if leaving {
			if sceneTempo > 0 {
				sceneTempo -= Game.delta
				if sceneTempo <= 0 {
					sceneTempo = 0
					setAlpha()
					Game.mode = modes[currentMode]
					Game.show(scene:GameScene.create())
				} else {
					setAlpha()
				}
			}
			return
		} else {
			if sceneTempo < 1 {
				sceneTempo += Game.delta
				if sceneTempo >= 1 {
					sceneTempo = 1
					setAlpha()
				} else {
					setAlpha()
					return
				}
			}
		}
		if modeSelect {
			if fadeTempo < 1 {
				fadeTempo += Game.delta
				if fadeTempo > 1 { fadeTempo = 1 }
				setAlpha()
			}
		} else {
			if fadeTempo > 0 {
				fadeTempo -= Game.delta
				if fadeTempo < 0 { fadeTempo = 0 }
				setAlpha()
			}
		}

        func playSFX(value:Int) {
            switch value {
            case 0: Game.audio.playSoundEffect(filename: "play.wav")
            case 1: Game.audio.playSoundEffect(filename: "nextMode.wav")
            case 2: Game.audio.playSoundEffect(filename: "previousMode.wav")
            default: break
            }
        }

		if modeSelect {
			if fadeTempo >= 0.9 {
				switch Game.input.swipe {
				case .left: selectMode(-1); playSFX(value: 2)
				case .right: selectMode(1); playSFX(value: 1)
				default: break
				}
				if Game.input.release(.click) || Game.input.release(.playPause) {
					leaving = true
                    playSFX(value: 0)
				}
			}
		} else {
			if fadeTempo <= 0.1 {
				if Game.input.release(.click) || Game.input.release(.playPause) {
                    modeSelect = true
                    playSFX(value: 0)
				}
			}
		}

		func setModePosition() {
			modeNodes[currentMode].position = CGPoint(x:-modeTempo*20,y:modeNodes[currentMode].position.y)
		}
		if modeTempo > 0 {
			modeTempo -= Game.delta*3
			if modeTempo < 0 { modeTempo = 0 }
			setModePosition()
		} else if modeTempo < 0 {
			modeTempo += Game.delta*3
			if modeTempo > 0 { modeTempo = 0 }
			setModePosition()
		}
		if leftTempo > 0 {
			leftTempo -= Game.delta*2
			if leftTempo < 0 { leftTempo = 0 }
			arrowLeftNode.color = Colour.lerp(from:Colour.gray,to:Colour.black,t:leftTempo*leftTempo*0.5)
		}
		if rightTempo > 0 {
			rightTempo -= Game.delta*2
			if rightTempo < 0 { rightTempo = 0 }
			arrowRightNode.color = Colour.lerp(from:Colour.gray,to:Colour.black,t:rightTempo*rightTempo*0.5)
		}
	}

	private func selectMode(_ move:Int,animate:Bool = true) {
		if animate {
			if move > 0 {
				modeTempo = 1
				rightTempo = 1
			} else {
				modeTempo = -1
				leftTempo = 1
			}
		}
		currentMode += move

		for (n,i) in modeNodes.enumerated() {
			i.alpha = (n == currentMode) ? 1 : 0
		}

		highScoreLabelNode.text = "\(Game.loadScore(for:modes[currentMode]))"
	}
}
