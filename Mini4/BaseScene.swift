import SpriteKit

class BaseScene:SKScene {
	private let cam = SKCameraNode()

	func defaults() -> SKScene {
		size = Game.size
		scaleMode = .aspectFit
		camera = cam
		return self
	}

	/*

	//you can make a create func in every child class like so:
	//(your scene file must be exactly 1920x1080)

	static func create() -> SKScene {
		return CustomScene(fileNamed:"CustomScene")!
	}

	//or if your class doesn't have a scene file, then:

	static func create() -> SKScene {
		return CustomScene().defaults()
	}

	*/

	override func sceneDidLoad() {
		start()
	}

	override func didMove(to view:SKView) {
		show()
	}

	override func update(_ currentTime:TimeInterval) {
		Game.update(currentTime)
		update()
		Game.lateUpdate()
	}

	func start() {
		//
	}

	func show() {
		//
	}

	func update() {
		//
	}
}
