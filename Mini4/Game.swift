import SpriteKit

class Game {
	static let me = Game()

	private(set) static var view:SKView!
	static let size = CGSize(width:1920,height:1080)

	static var bar = PlayerBarNode()
	static let room = RoomManager()
	static let input = RemoteInput()
	static let audio = SKTAudio()

	private static var firstUpdate = true
	private static var startTime:CGFloat = 0
	private(set) static var time:CGFloat = 0
	private(set) static var delta:CGFloat = 1/60

	private(set) static var scene:SKScene!

	//game state

	static var mode:GameMode = .comp
	static var inGame = false {
		didSet {
			room.lockPeers = inGame
		}
	}
	static let maxPlayers:Int = 5

	//control methods

	static func start(_ newView:SKView) {
		view = newView
		view.ignoresSiblingOrder = true
		view.showsFPS = false
		view.showsNodeCount = false
		input.start(view)
	}

	static func update(_ currentTime:TimeInterval) {
		if firstUpdate {
			firstUpdate = false
			startTime = CGFloat(currentTime)
			time = 0
			delta = 1/60
		} else {
			let newTime = CGFloat(currentTime)-startTime
			delta = newTime-time
			time = newTime
		}
		input.update()
		bar.update()
	}

	static func lateUpdate() {
		input.lateUpdate()
		room.lateUpdate()
	}

	//scene methods

	static func show(scene newScene:SKScene) {
		scene = newScene
		view.presentScene(scene)
		bar.attach(to:scene)
	}

	//saving methods

	static func loadScore(for mode:GameMode) -> Int {
		return (UserDefaults.standard.object(forKey:mode.rawValue) as? Int) ?? 0
	}

	static func saveScore(_ score:Int,for mode:GameMode) {
		UserDefaults.standard.set(score,forKey:mode.rawValue)
	}
}
