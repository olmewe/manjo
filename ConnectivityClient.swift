import Foundation
import MultipeerConnectivity

protocol ConnectivityClientDelegate {
	func updated(state:ConnectivityClientState)
	func received(command:String)
}

enum ConnectivityClientState {
	case lookingForServer
	case couldntFindServer
	case connected
	case disconnected
	case serverDown
}

class ConnectivityClient:Connectivity {
	private(set) var advertiser:MCNearbyServiceAdvertiser!
	fileprivate(set) var serverID:MCPeerID!
	private(set) var state:ConnectivityClientState = .disconnected

	var delegate:ConnectivityClientDelegate?

	init() {
		super.init(name:UIDevice.current.name)
		advertiser = MCNearbyServiceAdvertiser(peer:peerID,discoveryInfo:nil,serviceType:serviceType)
		advertiser.delegate = self
	}

	deinit {
		advertiser.stopAdvertisingPeer()
		endSession()
	}

	func lookForServer(timeout:Double = 5) {
		guard session == nil else { return }
		advertiser.startAdvertisingPeer()
		update(state:.lookingForServer)
		/*
		if #available(iOS 10,*) {
			Timer.scheduledTimer(withTimeInterval:timeout,repeats:false) {timer in
				self.stopLookingForServer()
			}
		} else {
			Timer.scheduledTimer(timeInterval:timeout,target:self,selector:#selector(self.stopLookingForServer),userInfo:nil,repeats:false)
		}
		*/
	}

	func stopLookingForServer() {
		advertiser.stopAdvertisingPeer()
		update(state:.couldntFindServer)
	}

	func disconnect() {
		endSession()
		update(state:.disconnected)
	}

	override internal func update(peer:MCPeerID,to state:MCSessionState) {
		guard serverID == peer else { return }
		if state == .notConnected {
			endSession()
			update(state:.serverDown)
		} else {
			log("server is \((state == .connected) ? "connected!" : "connecting...")")
		}
	}

	override internal func receive(command:String,from peer:MCPeerID) {
		guard serverID == peer else { return }
		delegate?.received(command:command)
	}

	func send(command:String) {
		guard let serverID = serverID else { return }
		send(command:command,to:[serverID])
	}

	fileprivate func update(state:ConnectivityClientState) {
		self.state = state
		delegate?.updated(state:state)
	}
}

extension ConnectivityClient:MCNearbyServiceAdvertiserDelegate {
	func advertiser(_ advertiser:MCNearbyServiceAdvertiser,didNotStartAdvertisingPeer error:Error) {
		log("couldn't start advertising this peer: \(error)")
	}

	func advertiser(_ advertiser:MCNearbyServiceAdvertiser,didReceiveInvitationFromPeer peerID:MCPeerID,withContext context:Data?,invitationHandler:@escaping (Bool,MCSession?) -> Void) {
		if session == nil {
			log("got invite from peer \(peerID)! sweet")
			beginSession()
			serverID = peerID
			advertiser.stopAdvertisingPeer()
			invitationHandler(true,session)
			update(state:.connected)
		} else {
			log("got invite from peer \(peerID)! but i'm already connected to another one...")
			invitationHandler(false,nil)
		}
	}
}
