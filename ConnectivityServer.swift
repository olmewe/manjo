import Foundation
import MultipeerConnectivity

protocol ConnectivityServerDelegate {
	func connected(peer:MCPeerID)
	func connecting(peer:MCPeerID)
	func disconnected(peer:MCPeerID)
	func received(command:String,from peer:MCPeerID)
}

class ConnectivityServer:Connectivity {
	private(set) var browser:MCNearbyServiceBrowser!

	var delegate:ConnectivityServerDelegate?

	init() {
		super.init(name:"Manjo Server")
		beginSession()
		browser = MCNearbyServiceBrowser(peer:peerID,serviceType:serviceType)
		browser.delegate = self
	}

	deinit {
		browser.stopBrowsingForPeers()
		endSession()
	}

	func start() {
		beginSession()
		browser.startBrowsingForPeers()
	}

	override internal func update(peer:MCPeerID,to state:MCSessionState) {
		switch state {
		case .connected: delegate?.connected(peer:peer)
		case .notConnected: delegate?.disconnected(peer:peer)
		case .connecting: delegate?.connecting(peer:peer)
		}
	}

	override internal func receive(command:String,from peer:MCPeerID) {
		delegate?.received(command:command,from:peer)
	}

	func send(command:String) {
		send(command:command,to:session.connectedPeers)
	}
}

extension ConnectivityServer:MCNearbyServiceBrowserDelegate {
	func browser(_ browser:MCNearbyServiceBrowser,didNotStartBrowsingForPeers error:Error) {
		log("couldn't start browsing for peers: \(error)")
	}

	func browser(_ browser:MCNearbyServiceBrowser,foundPeer peerID:MCPeerID,withDiscoveryInfo info:[String:String]?) {
		if let session = session {
			log("found a peer! let's invite them: \(peerID)")
			browser.invitePeer(peerID,to:session,withContext:nil,timeout:10)
		}
	}

	func browser(_ browser:MCNearbyServiceBrowser,lostPeer peerID:MCPeerID) {
		log("lost a peer! \(peerID)")
	}
}
